datalife_product_list_history
=============================

The product_list_history module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_list_history/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_list_history)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
